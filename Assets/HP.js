﻿#pragma strict
var life:int;
var oldlife:int;
var lifeprefab:GameObject;
var Stanbyframe:int =0;
var Stanbyframe2:int =0;
var createpos:Vector2;
var max = 3; //ライフの最大値

function Start () {
	createpos=Vector2(0.4,1.0);
	life = max;
	for(var i:int=0; i<life; i++){
		createLife(i);
	}
}
function Update () {
	
	if(life!=oldlife){ //ライフの加減処理
		if(life<oldlife){
			var delobj=GameObject.FindWithTag("life");
			//Debug.Log("delflag");
			if(delobj != null){
				Destroy(delobj);
			}
		}
			else if(life > oldlife){
				if(Stanbyframe2 > 0){
					createLife(life);	
				}
			}
	}	
	oldlife = life;
	if(Stanbyframe>0)
		Stanbyframe -= Time.deltaTime;
//		Debug.Log(Time.deltaTime);
	if(Stanbyframe2>0)
		Stanbyframe2 -= Time.deltaTime;
}

public function addLife(){
	if(Stanbyframe2 == 0){
		if(life < max ){
			life += 1;
			Stanbyframe2 +=180;
			return true;
		}
		return false;
		
	}
}
public function sublife(){
	if(Stanbyframe == 0){
		if(life > 1){
			life -= 1;
		}
		else {
			Application.LoadLevel("gameover");;
		}
		Stanbyframe +=180;
	}
}
function createLife(num:int){
	Debug.Log(num);
	var lifeobj=GameObject.Instantiate(lifeprefab);
	createpos += Vector2(0.02,0);
	lifeobj.transform.position = createpos;	
}