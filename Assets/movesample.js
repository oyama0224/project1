﻿#pragma strict

	var controller : CharacterController;//変数宣言
	var default_speed = 0.2;
	var move = Vector3(0,0,0);
	//var cameraVec = Vector3(0,0,0);
	var speed=default_speed;
	var m_camera:Camera;
	var s_camera:Camera;
	var jump:float;
	var firstjump = 3.0f;
	var kasokudo = 0.0001f;
	var gravity:float;
	var audioSource:AudioSource;
	public var audioClip:AudioClip; //再生ファイル名
	
	var stageManager:stageManager;
	var stagesize:Vector3;//移動範囲のためのマップサイズ
	
function Start () {
 	controller = GetComponent(CharacterController);//代入
 	m_camera = GameObject.FindWithTag("MainCamera").GetComponent("Camera");
 	s_camera = GameObject.Find("AimCamera").GetComponent("Camera");
	jump = 0.0f;
	gravity = 0.4f;
	audioSource = gameObject.GetComponent(AudioSource);
	audioSource.clip = audioClip;
	stageManager = GameObject.FindGameObjectWithTag("Stage").GetComponent("stageManager");
	stagesize = stageManager.getStageSize();
}

function Update () {
	var cameraForward:Vector3;
	var cameraRight:Vector3;
	if( m_camera.enabled== true){
		cameraForward = m_camera.transform.TransformDirection(Vector3.forward);
		cameraRight = m_camera.transform.TransformDirection(Vector3.right);
	}
	else{
		cameraForward = s_camera.transform.TransformDirection(Vector3.forward);
		cameraRight = s_camera.transform.TransformDirection(Vector3.right);
	}	
	if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0){
		if(audioSource.isPlaying == false){
			audioSource.Play();
		}
	}
	else if(Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0){
		if(audioSource.isPlaying == true){
			audioSource.Stop();
		}
	}
	if(Input.GetKey(KeyCode.LeftShift)){
		speed=default_speed*3;
	}
	else{
		speed=default_speed;
	}

	move = (Input.GetAxis("Horizontal")*speed)*cameraRight + (Input.GetAxis("Vertical")*speed)*cameraForward;
	move.y = 0;	
	
	if(Input.GetButtonDown("Jump") && controller.isGrounded){
		jump = firstjump;
	}
	else{
	move.y -= gravity;
	}
	
	if(jump > 0){
		audioSource.Stop();
		move += Vector3.up * jump;
		jump -= kasokudo;
	} else {
		jump = 0.0;
	}
	
	
	
	//move.z = Input.GetAxisRaw("Vertical") * speed * cameraRight;
	//move.x = Input.GetAxisRaw("Horizontal")* speed * cameraForward;
 	controller.Move(move*Time.deltaTime*60);
 	isCheckPos();
	transform.rotation.x = 0;
	transform.rotation.z = 0;
}

function isCheckPos(){
	
	var margin = 2.0f;
	if(transform.position.x > stagesize.x - margin)
		transform.position.x = stagesize.x - margin;
	
	else if(transform.position.x < 0 + margin)
		transform.position.x = 0 + margin;
		
	if(transform.position.z > stagesize.z - margin)
		transform.position.z = stagesize.z - margin;
	
	else if(transform.position.z < 0 + margin)
		transform.position.z = 0 + margin;
	
}

