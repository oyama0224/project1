﻿#pragma strict
var nvmsh:NavMeshAgent;
var steptime : float;
function Start () {
	nvmsh = GetComponent(NavMeshAgent);
	steptime = 0.5;
}

function Update () {
	if(steptime >= 0){
		nvmsh.speed = 1.5;
		steptime -= Time.deltaTime;
		if(steptime <= 0){
			steptime = -1;
		}
	}
	else {
		nvmsh.speed = 0;
		steptime += Time.deltaTime;
		if(steptime >= 0){
			steptime = 1;
		}
	}
}