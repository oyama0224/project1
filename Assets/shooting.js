﻿var spawn :Transform;
var speed :float= 10;
var bullet_obj : GameObject;
var maxbullet;//弾倉弾薬の最大数
var maxmag;
public var bullet:int;
public var mag:int;
var camera : Camera;
var splittime:float;
var cycletime:float;
var reloadtime:float;
var audioSource:AudioSource;
public var audioClip:AudioClip;
public var getsound:AudioClip;
public var Rsound:AudioClip;

function Start () {
	maxbullet = 4;
	maxmag = 16;
	bullet = 4;
	mag = 8;
	splittime = 0;
	cycletime = 0;
	reloadtime = 0;
	audioSource = gameObject.GetComponent("AudioSource");
}

function Update () {
	if(Input.GetButtonDown("Fire1")){
		Shot();
	}
	if(Input.GetKey(KeyCode.R)){
		Reload();
	}
	if(splittime > 0)
		splittime -= Time.deltaTime*60;
	if(cycletime > 0)
		cycletime -= Time.deltaTime*60;
	if(reloadtime > 0)
		reloadtime -= Time.deltaTime*60;
}

function Shot(){
	if(bullet > 0){
		if(cycletime <= 0){
			var obj : GameObject = GameObject.Instantiate(bullet_obj);  //GameObject型変数objを宣言
		    obj.transform.position = spawn.position;
		    obj.transform.rotation = gameObject.transform.rotation;
			obj.rigidbody.AddForce(gameObject.transform.forward*speed);
			bullet -=1;
			audioSource.PlayOneShot(audioClip);
			cycletime += 30;
		}
	}
}

function Reload(){
	if(reloadtime <= 0){
		if(bullet < maxbullet){
			if(0 < mag){
					bullet += 1;
					mag -= 1;
					reloadtime += 30;
					audioSource.PlayOneShot(Rsound);
			}
		}
	}
}
public function addMag(num:int){
	if(splittime <= 0){
		audioSource.PlayOneShot(getsound);
		mag += num;
		if(mag > maxmag){
			mag = maxmag;
		}
		splittime += 180;
	}
}