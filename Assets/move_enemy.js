﻿#pragma strict
var agent: NavMeshAgent;
var tgt :GameObject;
var anime:Animator;
function Start () {
	agent = gameObject.GetComponent(NavMeshAgent);
	tgt = GameObject.FindWithTag("Player");
	anime = GetComponent(Animator);
}
function Update () {
	if(anime != null){
		anime.SetBool("Walk",true);
	}
	agent.SetDestination((tgt.transform.position));
}