﻿#pragma strict
var m_camera:Camera;
var s_camera:Camera;
var m_audio:AudioListener;
var s_audio:AudioListener;
var flag;
var a:Transform;
function Start () {
	m_camera = GameObject.FindWithTag("MainCamera").GetComponent(Camera);
	m_audio = GameObject.FindWithTag("MainCamera").GetComponent(AudioListener);
	s_camera = GameObject.Find("AimCamera").GetComponent(Camera);
	s_audio = GameObject.Find("AimCamera").GetComponent(AudioListener);
	s_camera.enabled = false;
	s_audio.enabled = false;
}

function Update () {
	if(Input.GetMouseButtonDown(1)){
		if(m_camera.enabled == true){
			m_camera.enabled = false;
			m_audio.enabled = false;
			s_camera.enabled = true;
			s_audio.enabled = true;		
		}
	}
	else if(Input.GetMouseButtonUp(1)){
		if(s_camera.enabled == true){
			s_camera.enabled = false;
			s_audio.enabled = false;	
			m_camera.enabled = true;
			m_audio.enabled = true;
			}
	}
}

